class Nodo:
    def __init__(self, valor):
        self.valor = valor
        self.izquierda = None
        self.derecha = None

def insertar(raiz, valor):
    if raiz is None:
        return Nodo(valor)
    else:
        if valor < raiz.valor:
            raiz.izquierda = insertar(raiz.izquierda, valor)
        else:
            raiz.derecha = insertar(raiz.derecha, valor)
    return raiz

def inorder(raiz):
    if raiz:
        inorder(raiz.izquierda)
        print(raiz.valor, end=" ")
        inorder(raiz.derecha)

def preorder(raiz):
    if raiz:
        print(raiz.valor, end=" ")
        preorder(raiz.izquierda)
        preorder(raiz.derecha)

def posorder(raiz):
    if raiz:
        posorder(raiz.izquierda)
        posorder(raiz.derecha)
        print(raiz.valor, end=" ")

# Función para ingresar una serie de elementos
def ingresar_elementos():
    elementos = input("Ingrese los elementos separados por espacio: ").split()
    return list(map(int, elementos))

# Función principal
def main():
    elementos = ingresar_elementos()
    raiz = None
    for elemento in elementos:
        raiz = insertar(raiz, int(elemento))

    print("\nRecorrido en inorder:")
    inorder(raiz)

    print("\nRecorrido en preorder:")
    preorder(raiz)

    print("\nRecorrido en posorder:")
    posorder(raiz)

if __name__ == "__main__":
    main()
